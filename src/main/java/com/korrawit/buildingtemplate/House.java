/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.korrawit.buildingtemplate;

/**
 *
 * @author DELL
 */
public class House extends Buildingtemplate {

    public House(int numberOfRoom, int numberOfDoor, int numberOfWindow ,int numberOfFloor) {
        super(numberOfRoom, numberOfDoor, numberOfWindow , numberOfFloor);
        System.out.println("New House created");
    }

    @Override
    public void buildingDetail() {
        System.out.println("This House have " + numberOfRoom + " room(s) " + numberOfDoor + " door(s) and " + numberOfWindow + " window(s).");
        System.out.println("This House have " + numberOfFloor + " floor(s).");
    }

    @Override
    public void building(String name, String surname) {
        System.out.println("This House is registered by " + name + " " + surname + ".");
    }

    @Override
    public void building(String color) {
        System.out.println("This House have " + color + " color(s).");
    }

    @Override
    public void building(int numberOfCar) {
        System.out.println("This House have " + numberOfCar + " car garage slot.");
    }
}
