/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.korrawit.buildingtemplate;

/**
 *
 * @author DELL
 */
public class Buildingtemplate {

    protected int numberOfRoom;
    protected int numberOfDoor;
    protected int numberOfWindow;
    protected int numberOfFloor;

    public Buildingtemplate(int numberOfRoom, int numberOfDoor, int numberOfWindow ,int numberOfFloor) {
        System.out.println("New building Template created.");
        this.numberOfRoom = numberOfRoom;
        this.numberOfDoor = numberOfDoor;
        this.numberOfWindow = numberOfWindow;
        this.numberOfFloor = numberOfFloor;
    }

    public void buildingDetail() {
        System.out.println("This building have " + numberOfRoom + " room(s) " + numberOfDoor + " door(s) and " + numberOfWindow + " window(s).");
        System.out.println("This building have " + numberOfFloor + " floor(s).");
    }

    public void building(String name, String surname) {
        System.out.println("This building is registered by " + name + " " + surname + ".");
    }

    public void building(String color) {
        System.out.println("This building have " + color + " color(s).");
    }

    public void building(int numberOfCar) {
        System.out.println("This building have " + numberOfCar + " car garage slot.");
    }
}
