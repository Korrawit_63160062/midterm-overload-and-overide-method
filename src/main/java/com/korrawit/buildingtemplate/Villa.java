/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.korrawit.buildingtemplate;

/**
 *
 * @author DELL
 */
public class Villa extends Buildingtemplate {

    public Villa(int numberOfDoor, int numberOfWindow) {
        super(7, numberOfDoor, numberOfWindow , 2);
        System.out.println("New Villa created");
    }

    @Override
    public void buildingDetail() {
        System.out.println("This villa have " + numberOfRoom + " room(s) " + numberOfDoor + " door(s) and " + numberOfWindow + " window(s).");
        System.out.println("This villa have " + numberOfFloor + " floor(s).");
        System.out.println("Swimming pool included.");
    }

    @Override
    public void building(String name, String surname) {
        System.out.println("This villa is registered by " + name + " " + surname + ".");
    }

    @Override
    public void building(String color) {
        System.out.println("This villa have " + color + " color(s).");
    }

    @Override
    public void building(int numberOfCar) {
        System.out.println("This villa have " + numberOfCar + " car garage slot.");
    }
}
