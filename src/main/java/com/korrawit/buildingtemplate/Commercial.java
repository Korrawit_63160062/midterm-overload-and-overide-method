/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.korrawit.buildingtemplate;

/**
 *
 * @author DELL
 */
public class Commercial extends Buildingtemplate {
    public Commercial(int numberOfDoor, int numberOfWindow) {
        super(2, numberOfDoor, numberOfWindow, 3);
        System.out.println("New Commercial building created");
    }

    @Override
    public void buildingDetail() {
        System.out.println("This commercial building have " + numberOfRoom + " room(s) " + numberOfDoor + " door(s) and " + numberOfWindow + " window(s).");
        System.out.println("This commercial building have " + numberOfFloor + " floor(s).");
    }

    @Override
    public void building(String name, String surname) {
        System.out.println("This commercial building is registered by " + name + " " + surname + ".");
    }

    @Override
    public void building(String color) {
        System.out.println("This commercial building have " + color + " color(s).");
    }

    @Override
    public void building(int numberOfCar) {
        System.out.println("This commercial building have " + numberOfCar + " car garage slot.");
    }
}