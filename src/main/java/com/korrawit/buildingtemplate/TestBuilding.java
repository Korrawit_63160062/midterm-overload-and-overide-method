/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.korrawit.buildingtemplate;

/**
 * 
 * @author DELL
 */
public class TestBuilding {

    public static void main(String[] args) {
        Buildingtemplate building1 = new Buildingtemplate(5, 5, 10, 4);
        building1.buildingDetail();
        building1.building("Joe", "Biden");
        building1.building("Red & Blue & White");
        building1.building(10);

        Villa building2 = new Villa(7, 14);
        building2.buildingDetail();
        building2.building("Choi", "Yerim");
        building2.building("Purple");
        building2.building(1);
        
        Commercial building3 = new Commercial(6, 12);
        building3.buildingDetail();
        building3.building("S", "Coupe");
        building3.building("Grey");
        building3.building(2);
        
        House building4 = new House(2, 3, 6, 2);
        building4.buildingDetail();
        building4.building("Baek", "Jiheon");
        building4.building("Yellow");
        building4.building(0);
    }

}
